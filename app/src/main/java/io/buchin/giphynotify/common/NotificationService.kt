package io.buchin.giphynotify.common

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import io.buchin.giphynotify.R
import io.buchin.giphynotify.features.main.MainActivity
import io.buchin.giphynotify.interactors.NotificationState
import io.buchin.giphynotify.interactors.NetworkState
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.withLatestFrom
import space.traversal.kapsule.Injects
import space.traversal.kapsule.inject
import space.traversal.kapsule.required

class NotificationService : Service(), Injects<NotificationServiceModule> {
  private val networkState by required { networkState }
  private val imageInteractor by required { imageInteractor }
  private val settingsInteractor by required { settingsInteractor }
  private val notificationState by required { notificationState }
  private val disposeBag by required { disposeBag }

  private lateinit var serviceChannel: String
  private lateinit var notificationChannel: String

  override fun onCreate() {
    super.onCreate()

    inject(NotificationServiceModule(appModule))

    serviceChannel = getNotificationChannelId("NotificationService", "NotificationService")
    notificationChannel = getNotificationChannelId("NotificationChannel", "NotificationChannel")
  }

  override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
    val builder = NotificationCompat.Builder(this, serviceChannel)
      .setContentTitle("Сервис оповещения о получении картинок")
      .setContentText("Активно")
      .setSmallIcon(R.drawable.ic_launcher_background)
      .setPriority(NotificationCompat.PRIORITY_DEFAULT)

    startForeground(1, builder.build())

    imageInteractor.lastImage()
      .filter { settingsInteractor.getNotificationEnabled() }
      .withLatestFrom(notificationState)
      .filter { (_, appState) -> appState == NotificationState.SHOW }
      .map { (lastImage, _) -> lastImage }
      .withLatestFrom(networkState)
      .filter { (_, networkState) -> networkState == NetworkState.CONNECTED }
      .map { (lastImage, _) -> lastImage }
      .filter { lastImage -> lastImage.imageBitmap != null }
      .subscribe { showNotification() }
      .addTo(disposeBag)

    return START_NOT_STICKY
  }

  override fun onDestroy() {
    disposeBag.dispose()
    super.onDestroy()
  }

  override fun onBind(intent: Intent?): IBinder? {
    return null
  }

  private fun getNotificationChannelId(channelId: String, channelName: String): String {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      createNotificationChannel(channelId, channelName)
    } else {
      channelId
    }
  }

  @RequiresApi(Build.VERSION_CODES.O)
  private fun createNotificationChannel(channelId: String, channelName: String): String {
    val chan = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE)
    chan.lightColor = Color.BLUE
    chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
    val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    service.createNotificationChannel(chan)
    return channelId
  }

  private fun showNotification() {
    val builder = NotificationCompat.Builder(this, notificationChannel)
      .setContentTitle("Новая картинка")
      .setSmallIcon(R.drawable.ic_launcher_background)
      .setPriority(NotificationCompat.PRIORITY_DEFAULT)
      .apply {
        val pendingIntent = Intent(applicationContext, MainActivity::class.java)
          .let {
            PendingIntent.getActivity(applicationContext, 0, it, PendingIntent.FLAG_UPDATE_CURRENT)
          }

        setContentIntent(pendingIntent)
      }

    val notificationManager = NotificationManagerCompat.from(this)
    notificationManager.notify(2, builder.build())
  }
}
