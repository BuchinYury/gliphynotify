package io.buchin.giphynotify.common

import android.app.Activity
import android.app.Service
import android.graphics.Bitmap
import android.support.v4.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.buchin.giphynotify.GiphyNotifyApp
import io.buchin.giphynotify.features.main.MainActivity


val Activity.appModule get() = (application as GiphyNotifyApp).appModule
val Service.appModule get() = (application as GiphyNotifyApp).appModule
val Fragment.mainModule
  get() = (activity as MainActivity).mainModule

fun Bitmap?.compareToOtherBitmap(otherBtm: Bitmap?): Boolean {
  if (otherBtm == null && this == null) return true
  if (otherBtm == null) return false
  if (this == null) return false

  val gson = Gson()
  val a = gson.toJson(this)
  val b = gson.toJson(otherBtm)

  return a == b
}
