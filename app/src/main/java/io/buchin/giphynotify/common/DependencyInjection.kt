package io.buchin.giphynotify.common

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.annotation.IdRes
import android.support.v4.app.FragmentActivity
import com.giphy.sdk.core.network.api.GPHApiClient
import io.buchin.giphynotify.BuildConfig
import io.buchin.giphynotify.features.favouriteimages.FavouriteImagesAdapter
import io.buchin.giphynotify.features.favouriteimages.FavouriteImagesIntention
import io.buchin.giphynotify.features.favouriteimages.FavouriteImagesViewModel
import io.buchin.giphynotify.features.favouriteimages.FavouriteImagesViewState
import io.buchin.giphynotify.features.image.ImageViewModel
import io.buchin.giphynotify.features.image.ImageViewState
import io.buchin.giphynotify.features.main.MainViewModel
import io.buchin.giphynotify.features.main.MainViewState
import io.buchin.giphynotify.features.settings.SettingsViewModel
import io.buchin.giphynotify.features.settings.SettingsViewState
import io.buchin.giphynotify.interactors.ImageInteractor
import io.buchin.giphynotify.interactors.NetworkStateInteractor
import io.buchin.giphynotify.interactors.NotificationInteractor
import io.buchin.giphynotify.interactors.SettingsInteractor
import io.buchin.giphynotify.models.FavouriteImage
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

class AppModule(
  val context: Context
) {
  val cicerone: Cicerone<Router> = Cicerone.create()
  val navigatorHolder: NavigatorHolder = cicerone.navigatorHolder
  val router: Router = cicerone.router
  val sharedPreferences: SharedPreferences
    get() = context.getSharedPreferences(BuildConfig.sharedPreferencesName, Context.MODE_PRIVATE)
  private val giphyClient: GPHApiClient = GPHApiClient(BuildConfig.giphyApiKey)
  val settingsInteractor = SettingsInteractor(sharedPreferences)
  val networkStateInteractor = NetworkStateInteractor(context)
  val imageInteractor = ImageInteractor(
    giphyClient,
    networkStateInteractor
  )
  val notificationServiceInteractor = NotificationInteractor(
    context,
    Intent(context, NotificationService::class.java)
  )
}

class MainActivityModule(
  activity: FragmentActivity,
  @IdRes fragmentContainerId: Int,
  val appModule: AppModule
) {
  val disposeBag = CompositeDisposable()
  val navigatorHolder = appModule.navigatorHolder
  val navigator = MainActivityNavigator(activity, fragmentContainerId)
  val router = appModule.router
  val notificationServiceInteractor = appModule.notificationServiceInteractor
  val viewModel = MainViewModel(
    MainViewState(),
    notificationServiceInteractor,
    appModule.settingsInteractor,
    appModule.networkStateInteractor,
    disposeBag
  )
}

class NotificationServiceModule(appModule: AppModule) {
  val networkState = appModule.networkStateInteractor.networkState
  val imageInteractor = appModule.imageInteractor
  val settingsInteractor = appModule.settingsInteractor
  val notificationState = appModule.notificationServiceInteractor.notificationState
  val disposeBag = CompositeDisposable()
}

class ImageFragmentModule(
  displayWidth: Int,
  mainModule: MainActivityModule
) {
  val disposeBag = CompositeDisposable()
  val router = mainModule.router
  val viewModel = ImageViewModel(
    ImageViewState(resizeImageWidth = displayWidth),
    disposeBag,
    router,
    mainModule.appModule.imageInteractor,
    displayWidth
  )
}

class SettingsFragmentModule(
  mainModule: MainActivityModule
) {
  val disposeBag = CompositeDisposable()
  val router = mainModule.router
  val viewModel = SettingsViewModel(
    SettingsViewState(),
    disposeBag,
    router,
    mainModule.appModule.settingsInteractor,
    mainModule.appModule.notificationServiceInteractor
  )
}

class FavouriteImagesModule(
  mainModule: MainActivityModule
) {
  val disposeBag = CompositeDisposable()
  val router = mainModule.router
  val viewModel = FavouriteImagesViewModel(
    FavouriteImagesViewState(),
    disposeBag,
    router,
    mainModule.appModule.imageInteractor
  )
  val favouriteImagesAdapterProvider = { items: List<FavouriteImage> ->
    FavouriteImagesAdapter(items = items) {
      viewModel.intentions.onNext(FavouriteImagesIntention.DeleteImageFromFavouriteClicked(it))
    }
  }
}
