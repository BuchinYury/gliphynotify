package io.buchin.giphynotify.common

import android.content.Context
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import io.buchin.giphynotify.features.favouriteimages.FavouriteImagesFragment
import io.buchin.giphynotify.features.image.ImageFragment
import io.buchin.giphynotify.features.settings.SettingsFragment
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportAppNavigator

fun Router.navigateTo(screen: Screen) = navigateTo(screen::class.java.simpleName, screen)
fun Router.newRootScreen(screen: Screen) = newRootScreen(screen::class.java.simpleName, screen)

sealed class Screen {
  object ImageScreen : Screen()
  object SettingsScreen : Screen()
  object FavouriteImagesScreen : Screen()
}

class MainActivityNavigator(activity: FragmentActivity, @IdRes containerId: Int) :
    SupportAppNavigator(activity, containerId) {

  override fun createFragment(screenKey: String, data: Any?): Fragment = when (data) {
    Screen.ImageScreen -> ImageFragment()
    Screen.SettingsScreen -> SettingsFragment()
    Screen.FavouriteImagesScreen -> FavouriteImagesFragment()
    else -> throw IllegalArgumentException("Неизвестный экран: $screenKey")
  }

  override fun createActivityIntent(context: Context, screenKey: String, data: Any?) = null
}
