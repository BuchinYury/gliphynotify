package io.buchin.giphynotify.features.settings

import io.buchin.giphynotify.interactors.NotificationInteractor
import io.buchin.giphynotify.interactors.SettingsInteractor
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import ru.terrakok.cicerone.Router

class SettingsViewModel(
  initialState: SettingsViewState,
  disposeBag: CompositeDisposable,
  router: Router,
  settInteractor: SettingsInteractor,
  notificationInteractor: NotificationInteractor
) {
  val intentions: Subject<SettingsIntention> = PublishSubject.create()
  val sideEffects: Subject<SettingsSideEffect> = PublishSubject.create()
  val viewState: BehaviorSubject<SettingsViewState> = BehaviorSubject.createDefault(initialState)

  init {
    val loadNotificationEnabled = Observable.just(settInteractor.getNotificationEnabled())
      .map {
        { currentState: SettingsViewState ->
          val notificationEnabled = settInteractor.getNotificationEnabled()
          currentState.copy(notificationEnabled = notificationEnabled)
        }
      }

    val notificationCheckedChanges = intentions.filter { it is SettingsIntention.NotificationCheckedChanges }
      .map { (it as SettingsIntention.NotificationCheckedChanges).notificationEnabled }
      .doOnNext {
        sideEffects.onNext(SettingsSideEffect.NotificationCheckedChanges(it))
        if (it) sideEffects.onNext(SettingsSideEffect.StartNotificationService)
        else sideEffects.onNext(SettingsSideEffect.StopNotificationService)
      }
      .map {
        { currentState: SettingsViewState ->
          currentState.copy(notificationEnabled = it)
        }
      }

    Observable.merge(
      listOf(
        loadNotificationEnabled,
        notificationCheckedChanges
      )
    )
      .scan(initialState) { currentState, stateReducer ->
        stateReducer(currentState)
      }
      .subscribe(viewState)
  }

  init {
    intentions.filter { it === SettingsIntention.BackClicked }
      .map { SettingsSideEffect.GoBack }
      .subscribe(sideEffects)
  }

  init {
    sideEffects.filter { it === SettingsSideEffect.GoBack }
      .subscribe {
        router.exit()
      }
      .addTo(disposeBag)

    sideEffects.filter { it is SettingsSideEffect.NotificationCheckedChanges }
      .map { (it as SettingsSideEffect.NotificationCheckedChanges).notificationEnabled }
      .subscribe {
        settInteractor.setNotificationEnabled(it)
      }
      .addTo(disposeBag)

    sideEffects.filter { it === SettingsSideEffect.StartNotificationService }
      .subscribe {
        notificationInteractor.startService()
      }
      .addTo(disposeBag)

    sideEffects.filter { it === SettingsSideEffect.StopNotificationService }
      .subscribe {
        notificationInteractor.stopService()
      }
      .addTo(disposeBag)
  }
}

data class SettingsViewState(
  val notificationEnabled: Boolean = false
)

sealed class SettingsIntention {
  object ScreenResumed : SettingsIntention()
  object BackClicked : SettingsIntention()
  class NotificationCheckedChanges(val notificationEnabled: Boolean) : SettingsIntention()
}

sealed class SettingsSideEffect {
  object GoBack : SettingsSideEffect()
  object StartNotificationService : SettingsSideEffect()
  object StopNotificationService : SettingsSideEffect()
  class NotificationCheckedChanges(val notificationEnabled: Boolean) : SettingsSideEffect()
}
