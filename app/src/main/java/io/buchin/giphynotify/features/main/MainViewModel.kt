package io.buchin.giphynotify.features.main

import io.buchin.giphynotify.interactors.NetworkState
import io.buchin.giphynotify.interactors.NetworkStateInteractor
import io.buchin.giphynotify.interactors.NotificationInteractor
import io.buchin.giphynotify.interactors.SettingsInteractor
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class MainViewModel(
  initialState: MainViewState,
  notificationInteractor: NotificationInteractor,
  settingsInteractor: SettingsInteractor,
  networkStateInteractor: NetworkStateInteractor,
  disposeBag: CompositeDisposable
) {
  val intentions: Subject<MainIntention> = PublishSubject.create()
  val sideEffects: Subject<MainSideEffect> = PublishSubject.create()
  val viewState: Subject<MainViewState> = BehaviorSubject.createDefault(initialState)

  init {
    val screenResumed = intentions.filter { it is MainIntention.ScreenResumed }
      .withLatestFrom(networkStateInteractor.networkState) { _, networkState ->
        networkState
      }
      .map { networkState ->
        { currentState: MainViewState ->
          val errorMessage = if (settingsInteractor.getNotificationEnabled()) {
            "Невозможно загрузить данные из сети. Невозможен показ уведомлении."
          } else {
            "Невозможно загрузить данные из сети."
          }

          if (networkState == NetworkState.DISCONNECTED)
            currentState.copy(
              isImpossibleDownloadImage = true,
              errorMessage = errorMessage
            )
          else
            currentState.copy(
              isImpossibleDownloadImage = false,
              errorMessage = ""
            )
        }
      }
    val dismissImpossibleDownloadImageErrorDialog =
      intentions.filter { it == MainIntention.DismissImpossibleDownloadImageErrorDialog }
        .map {
          { currentState: MainViewState ->
            currentState.copy(
              isImpossibleDownloadImage = false,
              errorMessage = ""
            )
          }
        }

    Observable.merge(listOf(screenResumed, dismissImpossibleDownloadImageErrorDialog))
      .scan(initialState) { currentState, stateReduser ->
        stateReduser(currentState)
      }
      .subscribe(viewState)
  }

  init {
    intentions.filter { it === MainIntention.ScreenResumed }
      .subscribe {
        notificationInteractor.appVisible()
      }
      .addTo(disposeBag)

    intentions.filter { it === MainIntention.ScreenPaused }
      .subscribe {
        notificationInteractor.appHide()
      }
      .addTo(disposeBag)
  }

  init {
    if (settingsInteractor.getNotificationEnabled()) notificationInteractor.startService()
    else notificationInteractor.stopService()
  }
}

data class MainViewState(
  val isImpossibleDownloadImage: Boolean = false,
  val errorMessage: String = ""
)

sealed class MainIntention {
  object ScreenResumed : MainIntention()
  object ScreenPaused : MainIntention()
  object DismissImpossibleDownloadImageErrorDialog : MainIntention()
}

sealed class MainSideEffect
