package io.buchin.giphynotify.features.favouriteimages

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.buchin.giphynotify.R
import io.buchin.giphynotify.models.FavouriteImage
import kotlinx.android.synthetic.main.item_favourite_image.view.*

class FavouriteImagesAdapter(
  var items: List<FavouriteImage>,
  private val favouriteImageClickListener: (FavouriteImage) -> Unit
) : RecyclerView.Adapter<FavouriteImagesAdapter.SlugFiltersViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SlugFiltersViewHolder(
    LayoutInflater.from(parent.context)
      .inflate(R.layout.item_favourite_image, parent, false),
    favouriteImageClickListener
  )

  override fun onBindViewHolder(holder: SlugFiltersViewHolder, position: Int) = holder.bind(items[position])

  override fun getItemCount() = items.size

  inner class SlugFiltersViewHolder(
    view: View,
    private val favouriteImageClickListener: (FavouriteImage) -> Unit
  ) : RecyclerView.ViewHolder(view) {
    fun bind(favouriteImage: FavouriteImage) {
      itemView.apply {
        imageview_favourite_image_favourite?.setOnClickListener {
          favouriteImageClickListener(favouriteImage)
        }

        imageview_favourite_image?.setImageBitmap(favouriteImage.favouriteImage)
      }
    }
  }
}
