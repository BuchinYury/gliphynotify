package io.buchin.giphynotify.features.favouriteimages

import io.buchin.giphynotify.interactors.ImageInteractor
import io.buchin.giphynotify.models.FavouriteImage
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import ru.terrakok.cicerone.Router

class FavouriteImagesViewModel(
  initialState: FavouriteImagesViewState,
  disposeBag: CompositeDisposable,
  router: Router,
  imageInteractor: ImageInteractor
) {
  val intentions: Subject<FavouriteImagesIntention> = PublishSubject.create()
  val sideEffects: Subject<FavouriteImagesSideEffect> = PublishSubject.create()
  val viewState: BehaviorSubject<FavouriteImagesViewState> = BehaviorSubject.createDefault(initialState)

  init {
    val favouriteImages = Observable.create<List<FavouriteImage>> {
      it.onNext(imageInteractor.getFavouriteImages())
    }
      .map {
        { currentState: FavouriteImagesViewState ->
          currentState.copy(favouriteImages = it)
        }
      }

    val deleteImageFromFavouriteClicked =
      intentions.filter { it is FavouriteImagesIntention.DeleteImageFromFavouriteClicked }
        .map { (it as FavouriteImagesIntention.DeleteImageFromFavouriteClicked).favouriteImages }
        .doOnNext { sideEffects.onNext(FavouriteImagesSideEffect.DeleteImageFromFavourite(it)) }
        .map { favouriteImagesToDelete ->
          { currentState: FavouriteImagesViewState ->
            val newFavouriteImages = currentState.favouriteImages
              .filter {
                it != favouriteImagesToDelete
              }
            currentState.copy(favouriteImages = newFavouriteImages)
          }
        }

    Observable.merge(
      listOf(
        favouriteImages,
        deleteImageFromFavouriteClicked
      )
    )
      .scan(initialState) { currentState, stateReducer ->
        stateReducer(currentState)
      }
      .subscribe(viewState)
  }

  init {
    intentions.filter { it === FavouriteImagesIntention.BackClicked }
      .map { FavouriteImagesSideEffect.GoBack }
      .subscribe(sideEffects)
  }

  init {
    sideEffects.filter { it === FavouriteImagesSideEffect.GoBack }
      .subscribe { router.exit() }
      .addTo(disposeBag)

    sideEffects.filter { it is FavouriteImagesSideEffect.DeleteImageFromFavourite }
      .map { (it as FavouriteImagesSideEffect.DeleteImageFromFavourite).favouriteImages }
      .subscribe {
        imageInteractor.removeImageFromFavourite(it.favouriteImageName)
      }
      .addTo(disposeBag)
  }
}

data class FavouriteImagesViewState(
  val favouriteImages: List<FavouriteImage> = emptyList(),
  val error: Throwable? = null
)

sealed class FavouriteImagesIntention {
  object BackClicked : FavouriteImagesIntention()
  class DeleteImageFromFavouriteClicked(val favouriteImages: FavouriteImage) : FavouriteImagesIntention()
}

sealed class FavouriteImagesSideEffect {
  object GoBack : FavouriteImagesSideEffect()
  class DeleteImageFromFavourite(val favouriteImages: FavouriteImage) : FavouriteImagesSideEffect()
}
