package io.buchin.giphynotify.features.main

import android.Manifest
import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import io.buchin.giphynotify.GiphyNotifyApp
import io.buchin.giphynotify.R
import io.buchin.giphynotify.common.MainActivityModule
import io.buchin.giphynotify.common.Screen
import io.buchin.giphynotify.common.newRootScreen
import io.reactivex.rxkotlin.addTo
import space.traversal.kapsule.Injects
import space.traversal.kapsule.inject
import space.traversal.kapsule.required


class MainActivity : AppCompatActivity(), Injects<MainActivityModule> {
  val mainModule by lazy {
    MainActivityModule(
      this,
      R.id.framelayout_main_fragment_container,
      (application as GiphyNotifyApp).appModule
    )
  }

  // region dependency injection
  private val disposeBag by required { disposeBag }
  private val navigatorHolder by required { navigatorHolder }
  private val navigator by required { navigator }
  private val router by required { router }
  private val viewModel by required { viewModel }
  // endregion

  // region activity
  override fun onCreate(savedInstanceState: Bundle?) {
    setTheme(R.style.AppTheme)
    super.onCreate(savedInstanceState)
    inject(mainModule)
    setContentView(R.layout.activity_main)
    router.newRootScreen(Screen.ImageScreen)
    handleImpossibleDownloadImage()
    requestPermissions()
  }

  override fun onResumeFragments() {
    super.onResumeFragments()
    navigatorHolder.setNavigator(navigator)
  }

  override fun onResume() {
    super.onResume()
    viewModel.intentions.onNext(MainIntention.ScreenResumed)
  }

  override fun onPause() {
    super.onPause()
    navigatorHolder.removeNavigator()
    viewModel.intentions.onNext(MainIntention.ScreenPaused)
  }

  override fun onDestroy() {
    super.onDestroy()
    disposeBag.dispose()
  }
  // endregion

  // region handle
  private fun handleImpossibleDownloadImage() {
    viewModel.viewState
      .distinctUntilChanged { currentState, newState ->
        currentState.isImpossibleDownloadImage == newState.isImpossibleDownloadImage
      }
      .filter { it.isImpossibleDownloadImage }
      .subscribe {
        AlertDialog.Builder(this)
          .setCancelable(false)
          .setTitle(R.string.dialog_impossible_download_image_title)
          .setMessage(it.errorMessage)
          .setPositiveButton(R.string.dialog_error_positive_button_text) { _, _ ->
            viewModel.intentions.onNext(MainIntention.DismissImpossibleDownloadImageErrorDialog)
          }
          .create()
          .show()
      }
      .addTo(disposeBag)
  }
  // endregion

  // region permissions
  private fun requestPermissions() {
    val arrayOfPermissions =
      arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)

    ActivityCompat.requestPermissions(
      this,
      arrayOfPermissions,
      PERMISSION_REQUEST_CODE
    )
  }
  // endregion

  companion object {
    private const val PERMISSION_REQUEST_CODE = 57
  }
}
