package io.buchin.giphynotify.features.image

import android.graphics.Bitmap
import io.buchin.giphynotify.common.Screen
import io.buchin.giphynotify.common.navigateTo
import io.buchin.giphynotify.interactors.ImageInteractor
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import ru.terrakok.cicerone.Router

class ImageViewModel(
  initialState: ImageViewState,
  disposeBag: CompositeDisposable,
  router: Router,
  imageInteractor: ImageInteractor,
  displayWidth: Int
) {
  val intentions: Subject<ImageIntention> = PublishSubject.create()
  val sideEffects: Subject<ImageSideEffect> = PublishSubject.create()
  val viewState: BehaviorSubject<ImageViewState> = BehaviorSubject.createDefault(initialState)

  init {
    val lastImage = imageInteractor.lastImage()
      .map {
        if (it.imageBitmap != null) {
          { currentState: ImageViewState ->
            currentState.copy(
              image = it.imageBitmap,
              resizeImageHeight = displayWidth / it.imageBitmap.width * it.imageBitmap.height
            )
          }
        } else {
          { currentState: ImageViewState ->
            currentState.copy(error = it.error)
          }
        }
      }
    val dismissErrorDialog = intentions.filter { it === ImageIntention.DismissErrorDialog }
      .map {
        { currentState: ImageViewState ->
          currentState.copy(error = null)
        }
      }

    Observable.merge(
      listOf(
        lastImage,
        dismissErrorDialog
      )
    )
      .scan(initialState) { currentState, stateReducer ->
        stateReducer(currentState)
      }
      .subscribe(viewState)
  }

  init {
    intentions.filter { it === ImageIntention.SettingsClicked }
      .map { ImageSideEffect.NavigateToSettingsScreen }
      .subscribe(sideEffects)

    intentions.filter { it === ImageIntention.FavouritesClicked }
      .map { ImageSideEffect.NavigateToFavouritesScreen }
      .subscribe(sideEffects)

    intentions.filter { it === ImageIntention.AddImageToFavouritesClicked }
      .map { ImageSideEffect.AddImageToFavorites }
      .subscribe(sideEffects)
  }

  init {
    sideEffects.filter { it === ImageSideEffect.NavigateToSettingsScreen }
      .subscribe { router.navigateTo(Screen.SettingsScreen) }
      .addTo(disposeBag)

    sideEffects.filter { it === ImageSideEffect.NavigateToFavouritesScreen }
      .subscribe { router.navigateTo(Screen.FavouriteImagesScreen) }
      .addTo(disposeBag)

    sideEffects.filter { it === ImageSideEffect.AddImageToFavorites }
      .withLatestFrom(viewState) { _, viewState -> viewState }
      .filter { it.image != null }
      .subscribe { imageInteractor.addImageToFavourites(it.image as Bitmap) }
      .addTo(disposeBag)
  }
}

data class ImageViewState(
  val image: Bitmap? = null,
  val resizeImageHeight: Int = 0,
  val resizeImageWidth: Int = 0,
  val error: Throwable? = null
)

sealed class ImageIntention {
  object SettingsClicked : ImageIntention()
  object FavouritesClicked : ImageIntention()
  object DismissErrorDialog : ImageIntention()
  object AddImageToFavouritesClicked : ImageIntention()
}

sealed class ImageSideEffect {
  object NavigateToSettingsScreen : ImageSideEffect()
  object NavigateToFavouritesScreen : ImageSideEffect()
  object AddImageToFavorites : ImageSideEffect()
}
