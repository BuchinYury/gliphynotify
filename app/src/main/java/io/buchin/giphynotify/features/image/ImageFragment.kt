package io.buchin.giphynotify.features.image

import android.app.AlertDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.visibility
import io.buchin.giphynotify.R
import io.buchin.giphynotify.common.ImageFragmentModule
import io.buchin.giphynotify.common.mainModule
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_image.*
import space.traversal.kapsule.Injects
import space.traversal.kapsule.inject
import space.traversal.kapsule.required


class ImageFragment : Fragment(), Injects<ImageFragmentModule> {
  // region dependency injection
  private val viewModel by required { viewModel }
  private val disposeBag by required { disposeBag }
  // endregion

  // region Fragment
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    inject(
      ImageFragmentModule(
        displayWidth(),
        mainModule
      )
    )
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View = inflater.inflate(R.layout.fragment_image, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    handleError()
    handleImage()
    handleSettings()
    handleFavourites()
  }

  override fun onDestroy() {
    super.onDestroy()
    disposeBag.dispose()
  }
  // endregion

  // regionHandle
  private fun handleImage() {
    viewModel.viewState
      .filter { it.image != null }
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe { viewState ->
        with(viewState) {
          val scaleImage = Bitmap.createScaledBitmap(image as Bitmap, resizeImageWidth, resizeImageHeight, false)
          imageview_image_fragment_image?.setImageBitmap(scaleImage)
        }
      }
      .addTo(disposeBag)

    viewModel.viewState
      .map { it.image != null }
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(imageview_image_fragment_favourite.visibility())
      .addTo(disposeBag)
  }

  private fun handleSettings() {
    imageview_toolbar_fragment_settings.clicks()
      .map { ImageIntention.SettingsClicked }
      .subscribe(viewModel.intentions)
  }

  private fun handleFavourites() {
    imageview_toolbar_fragment_favourites.clicks()
      .map { ImageIntention.FavouritesClicked }
      .subscribe(viewModel.intentions)

    imageview_image_fragment_favourite.clicks()
      .map { ImageIntention.AddImageToFavouritesClicked }
      .subscribe(viewModel.intentions)
  }

  private fun handleError() {
    viewModel.viewState
      .filter { it.error != null }
      .map { it.error?.message ?: it.error.toString() }
      .subscribe {
        AlertDialog.Builder(context)
          .setCancelable(false)
          .setTitle(R.string.dialog_error_title)
          .setPositiveButton(R.string.dialog_error_positive_button_text) { _, _ ->
            viewModel.intentions.onNext(ImageIntention.DismissErrorDialog)
          }
          .create()
          .show()
      }
      .addTo(disposeBag)
  }
  // endregion

  private fun displayWidth(): Int {
    val metrics = DisplayMetrics()
    requireActivity().windowManager.defaultDisplay.getMetrics(metrics)
    return metrics.widthPixels
  }
}
