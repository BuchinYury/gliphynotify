package io.buchin.giphynotify.features.favouriteimages

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import io.buchin.giphynotify.R
import io.buchin.giphynotify.common.FavouriteImagesModule
import io.buchin.giphynotify.common.mainModule
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_favourite_images.*
import space.traversal.kapsule.Injects
import space.traversal.kapsule.inject
import space.traversal.kapsule.required


class FavouriteImagesFragment : Fragment(), Injects<FavouriteImagesModule> {
  // region dependency injection
  private val viewModel by required { viewModel }
  private val disposeBag by required { disposeBag }
  private val favouriteImagesAdapterProvider by required { favouriteImagesAdapterProvider }
  // endregion

  // region Fragment
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    inject(FavouriteImagesModule(mainModule))
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View = inflater.inflate(R.layout.fragment_favourite_images, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    handleBack()
    handleFavouriteImages()
  }

  override fun onDestroy() {
    super.onDestroy()
    disposeBag.dispose()
  }
  // endregion

  // regionHandle
  private fun handleBack() {
    imageview_favourite_images_toolbar_back.clicks()
      .map { FavouriteImagesIntention.BackClicked }
      .subscribe(viewModel.intentions)
  }

  private fun handleFavouriteImages() {
    recyclerview_favourite_images?.apply {
      setHasFixedSize(true)
      isNestedScrollingEnabled = false
      layoutManager = LinearLayoutManager(context, 1, false)
      adapter = favouriteImagesAdapterProvider(emptyList())
    }

    viewModel.viewState
      .map { it.favouriteImages }
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe {
        recyclerview_favourite_images?.apply {
          (adapter as? FavouriteImagesAdapter)?.items = it
          adapter?.notifyDataSetChanged()
        }
      }
      .addTo(disposeBag)
  }
  // endregion
}
