package io.buchin.giphynotify.features.settings

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.checked
import com.jakewharton.rxbinding2.widget.checkedChanges
import io.buchin.giphynotify.R
import io.buchin.giphynotify.common.SettingsFragmentModule
import io.buchin.giphynotify.common.mainModule
import kotlinx.android.synthetic.main.fragment_settings.*
import space.traversal.kapsule.Injects
import space.traversal.kapsule.inject
import space.traversal.kapsule.required

class SettingsFragment : Fragment(), Injects<SettingsFragmentModule> {
  // region dependency injection
  private val viewModel by required { viewModel }
  private val disposeBag by required { disposeBag }
  // endregion

  // region Fragment
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    inject(SettingsFragmentModule(mainModule))
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View = inflater.inflate(R.layout.fragment_settings, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    handleBack()
    handleNotification()
  }

  override fun onResume() {
    super.onResume()
    viewModel.intentions.onNext(SettingsIntention.ScreenResumed)
  }

  override fun onDestroy() {
    super.onDestroy()
    disposeBag.dispose()
  }
  // endregion

  // regionHandle
  private fun handleBack() {
    imageview_settings_toolbar_back.clicks()
      .map { SettingsIntention.BackClicked }
      .subscribe(viewModel.intentions)
  }

  private fun handleNotification() {
    switchcompat_settings_notification?.apply {
      viewModel.viewState
        .map { it.notificationEnabled }
        .filter { it != isChecked }
        .subscribe(checked())

      checkedChanges().map { SettingsIntention.NotificationCheckedChanges(it) }
        .subscribe(viewModel.intentions)
    }
  }
  // endregion
}
