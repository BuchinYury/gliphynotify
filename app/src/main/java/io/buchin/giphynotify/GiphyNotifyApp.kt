package io.buchin.giphynotify

import android.app.Application
import android.content.IntentFilter
import android.net.ConnectivityManager
import io.buchin.giphynotify.common.AppModule

class GiphyNotifyApp : Application() {
  val appModule by lazy {
    AppModule(this)
  }
}