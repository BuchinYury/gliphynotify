package io.buchin.giphynotify.models

import android.graphics.Bitmap

data class FavouriteImage(
  val favouriteImageName: String,
  val favouriteImage: Bitmap
)