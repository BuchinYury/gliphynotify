package io.buchin.giphynotify.models

import android.graphics.Bitmap

data class GiphyImageOrError(
  val imageBitmap: Bitmap? = null,
  val error: Throwable? = null
)