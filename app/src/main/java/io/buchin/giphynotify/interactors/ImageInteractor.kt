package io.buchin.giphynotify.interactors

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import com.giphy.sdk.core.models.enums.MediaType
import com.giphy.sdk.core.network.api.GPHApi
import io.buchin.giphynotify.common.compareToOtherBitmap
import io.buchin.giphynotify.models.FavouriteImage
import io.buchin.giphynotify.models.GiphyImageOrError
import io.reactivex.Observable
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.net.URL
import java.util.concurrent.TimeUnit

class ImageInteractor(
  private val giphyClient: GPHApi,
  networkStateReceiver: NetworkStateInteractor
) {
  private val imageSubject: BehaviorSubject<GiphyImageOrError> = BehaviorSubject.create()
  private val imageUpdatePeriod = 10L
  private val rootImagePath = "${Environment.getExternalStorageDirectory().absolutePath}/saved_images"
  private val favoritesPath = "$rootImagePath/favorites"
  private val lastImageName = "last_image"

  init {
    Observable.interval(0, imageUpdatePeriod, TimeUnit.SECONDS)
      .withLatestFrom(networkStateReceiver.networkState)
      .subscribe { (_, networkState) ->
        if (networkState == NetworkState.CONNECTED)
          loadLastImageFromGiphy()
        else
          loadLastImageFromDisk()
      }
  }

  fun lastImage(): Observable<GiphyImageOrError> = imageSubject
    .distinctUntilChanged { currentState, newState ->
      currentState.imageBitmap.compareToOtherBitmap(newState.imageBitmap)
    }

  fun getFavouriteImages(): List<FavouriteImage> = File(favoritesPath).listFiles()
    ?.takeIf { it.isNotEmpty() }
    ?.map {
      FavouriteImage(
        favouriteImageName = it.nameWithoutExtension,
        favouriteImage = BitmapFactory.decodeStream(FileInputStream(it))
      )
    }
    ?: emptyList()

  fun addImageToFavourites(image: Bitmap) {
    val myDir = File(favoritesPath)
    myDir.mkdirs()

    if (!equalsImageToFavouriteImages(image)) {
      File(favoritesPath).listFiles()?.apply {
        if (lastIndex >= 0) {
          val imageName = try {
            (this[lastIndex].nameWithoutExtension.toInt() + 1).toString()
          } catch (_: Throwable) {
            "0"
          }
          saveImageToFavoritesDir(image, imageName)
        } else
          saveImageToFavoritesDir(image, "0")
      }
    }
  }

  fun removeImageFromFavourite(imageName: String) {
    removeImageFromPath(favoritesPath, imageName)
  }

  private fun removeImageFromPath(path: String, imageName: String) {
    File(path).listFiles()
      ?.filter { it.nameWithoutExtension == imageName }
      ?.forEach { it.delete() }
  }

  private fun equalsImageToFavouriteImages(image: Bitmap): Boolean = try {
    val imageForEqualsName = "image_for_equals"
    saveImage(image, rootImagePath, imageForEqualsName)

    val imageBaos = ByteArrayOutputStream()
    BitmapFactory.decodeFile("$rootImagePath/$imageForEqualsName.jpg")
      .compress(Bitmap.CompressFormat.PNG, 100, imageBaos)

    val imageEqualsFavoriteImages = File(favoritesPath).listFiles()
      ?.takeIf { it.isNotEmpty() }
      ?.filter {
        val lastImageAddToFavouriteBaos = ByteArrayOutputStream()
        BitmapFactory.decodeFile(it.path)
          .compress(Bitmap.CompressFormat.PNG, 100, lastImageAddToFavouriteBaos)
        imageBaos.toByteArray()?.contentEquals(lastImageAddToFavouriteBaos.toByteArray()) ?: false
      }
      ?.let { it.isNotEmpty() }
      ?: false

    imageEqualsFavoriteImages.apply {
      removeImageFromPath(rootImagePath, imageForEqualsName)
    }
  } catch (_: Throwable) {
    false
  }

  private fun loadLastImageFromDisk() {
    val bitmap = BitmapFactory.decodeFile("$rootImagePath/$lastImageName.jpg")
    imageSubject.onNext(GiphyImageOrError(imageBitmap = bitmap))
  }

  private fun loadLastImageFromGiphy() {
    giphyClient.trending(
      MediaType.gif,
      1,
      null,
      null
    ) { result, e ->
      when {
        e != null -> GiphyImageOrError(error = e)
        result.data != null -> {
          result?.data
            ?.map { it.images.original }
            ?.takeIf { it.isNotEmpty() }
            ?.get(0)
            ?.apply {
              val gifUrl = this.gifUrl
              GlobalScope.launch {
                val url = URL(gifUrl)
                val bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                saveLastImage(bmp, "last_image")
                imageSubject.onNext(GiphyImageOrError(imageBitmap = bmp))
              }
            }
        }
      }
    }
  }

  private fun isExternalStorageWritable(): Boolean = Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()

  private fun saveLastImage(btm: Bitmap, imageName: String) {
    if (isExternalStorageWritable()) {
      saveImage(btm, rootImagePath, imageName)
    }
  }

  private fun saveImageToFavoritesDir(btm: Bitmap, imageName: String) {
    if (isExternalStorageWritable()) {
      saveImage(btm, favoritesPath, imageName)
    }
  }

  private fun saveImage(btm: Bitmap, dir: String, imageName: String) {
    val myDir = File(dir)
    myDir.mkdirs()
    val file = File(dir, "$imageName.jpg")
    if (file.exists()) file.delete()
    try {
      val out = FileOutputStream(file)
      btm.compress(Bitmap.CompressFormat.PNG, 100, out)
      out.flush()
      out.close()
    } catch (_: Exception) {
    }
  }
}

