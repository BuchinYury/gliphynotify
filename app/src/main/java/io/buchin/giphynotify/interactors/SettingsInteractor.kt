package io.buchin.giphynotify.interactors

import android.content.SharedPreferences

private const val keyIsNotificationEnable = "KEY_IsNotificationEnable"

class SettingsInteractor(private val sharedPreferences: SharedPreferences) {
  fun getNotificationEnabled() =
    sharedPreferences.getBoolean(keyIsNotificationEnable, false)

  fun setNotificationEnabled(isNotificationEnable: Boolean) {
    sharedPreferences.edit()
      .putBoolean(keyIsNotificationEnable, isNotificationEnable)
      .apply()
  }
}