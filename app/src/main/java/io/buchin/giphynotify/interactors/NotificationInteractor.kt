package io.buchin.giphynotify.interactors

import android.content.Context
import android.content.Intent
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

enum class NotificationState {
  SHOW,
  DONT_SHOW
}

class NotificationInteractor(
  private val context: Context,
  private val serviceIntent: Intent
) {
  private val notificationStateSubject: Subject<NotificationState> =
    BehaviorSubject.createDefault(NotificationState.SHOW)
  val notificationState: Observable<NotificationState> = notificationStateSubject

  fun appVisible() {
    notificationStateSubject.onNext(NotificationState.DONT_SHOW)
  }

  fun appHide() {
    notificationStateSubject.onNext(NotificationState.SHOW)
  }

  fun startService() {
    context.startService(serviceIntent)
  }

  fun stopService() {
    context.stopService(serviceIntent)
  }
}