package io.buchin.giphynotify.interactors

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject


enum class NetworkState {
  CONNECTED,
  DISCONNECTED,
}

class NetworkStateInteractor(context: Context) {
  private val networkSubject: Subject<NetworkState> = BehaviorSubject.createDefault(NetworkState.DISCONNECTED)
  val networkState: Observable<NetworkState> = networkSubject

  init {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    val builder = NetworkRequest.Builder()

    connectivityManager?.registerNetworkCallback(
      builder.build(),
      object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
          networkSubject.onNext(NetworkState.CONNECTED)
        }

        override fun onLost(network: Network?) {
          networkSubject.onNext(NetworkState.DISCONNECTED)
        }
      }
    )
  }
}